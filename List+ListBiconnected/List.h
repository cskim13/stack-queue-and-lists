#pragma once
template<typename T>
class Node
{
public:
	T Key;
	Node *Next;
};


template<typename T>
class List
{
	Node<T> *Head;

public:
	List() { Head = NULL; }

	void Add(T _Key)
	{
		Node<T> *temp = new Node<T>;
		temp->Key = _Key;
		temp->Next = Head;
		Head = temp;
	}

	void Delete(T _Key)
	{
		Node<T> *tmp1 = Head;
		Node<T> *tmp2;
		if (tmp1->Key != _Key)
		{
			while (tmp1->Next->Key != _Key) tmp1 = tmp1->Next;
			tmp2 = tmp1->Next;
			tmp1->Next = tmp1->Next->Next;
			delete tmp2;
		}
		else
		{
			Head = tmp1->Next;
			delete tmp1;
		}
	}

	void checkEmpty() {
		if (Head == NULL) cout << "List is empty" << endl;
		else cout << "List is not empty" << endl;
	}

	int getCount() {
		int count = 0;
		Node<T> *tmp = Head;
		while (tmp != NULL) {
			count++;
			tmp = tmp->Next;
		}
		return count;
	}

	void Print()
	{
		Node<T> *temp = Head;
		cout << "\nPrint list:                         " << endl;
		while (temp != NULL)
		{
			cout << temp->Key << " ";
			temp = temp->Next;
		}
		cout << endl << " " << endl;
	}

	void Swap(T _Key1, T _Key2)
	{
		Node<T> *Node1 = Head;
		Node<T> *Node2 = Head;

		while (Node1->Key != _Key1) Node1 = Node1->Next;
		while (Node2->Key != _Key2) Node2 = Node2->Next;

		T tmp = Node1->Key;
		Node1->Key = Node2->Key;
		Node2->Key = tmp;
	}

	void DeleteList() {
		Node<T> *tmp1 = Head;
		Node<T> *tmp2;

		while (tmp1 != NULL) {
			tmp2 = tmp1;
			tmp1 = tmp1->Next;
			delete tmp2;
		}

		Head = NULL;
	}

	void AddToEnd(T _Key) {
		Node<T> *tmp = Head;
		while (tmp->Next != NULL) tmp = tmp->Next;
		Node<T> *node = new Node<T>;
		node->Key = _Key;
		tmp->Next = node;
		node->Next = NULL;
	}

	void Sort() {

		Node<T> *Node1;
		Node<T> *Node2;

		for (int i = 0; i < getCount(); i++)
		{
			Node1 = Head;
			Node2 = Node1->Next;

			while (Node2 != NULL)
			{
				if (Node1->Key > Node2->Key)
				{
					Swap(Node1->Key, Node2->Key);
				}
				Node1 = Node1->Next;
				Node2 = Node2->Next;
			}
		}
	}

void Merge(List & B) {

		Node<T> *tmp = B.Head;

		while (tmp != NULL)
		{
			AddToEnd(tmp->Key);
			tmp = tmp->Next;
		}
	}
};