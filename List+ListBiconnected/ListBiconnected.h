
#pragma once

template<typename T>
class BiNode
{
public:
	T Key;
	BiNode *Next;
	BiNode *Prev;
};


template<typename T>
class ListBi
{
	BiNode<T> *Head;

public:
	ListBi() { Head = NULL; }

	void Add(T _Key)
	{
		BiNode<T> *temp = new BiNode<T>;
		temp->Key = _Key;
		temp->Next = Head;

		if (Head != NULL) temp->Next->Prev = temp;
		temp->Prev = NULL;

		Head = temp;
	}

	void Delete(T _Key)
	{
		BiNode<T> *tmp1 = Head;
		BiNode<T> *tmp2;
		if (tmp1->Key != _Key)
		{
			while (tmp1->Next->Key != _Key) tmp1 = tmp1->Next;
			tmp2 = tmp1->Next;
			tmp1->Next = tmp1->Next->Next;
			if (tmp2->Next != NULL) tmp2->Next->Prev = tmp1;
			delete tmp2;
		}
		else
		{
			Head = tmp1->Next;
			Head->Prev = NULL;
			delete tmp1;
		}
	}

	void CheckEmpty() {
		if (Head == NULL) cout << "List is empty" << endl;
		else cout << "List is not empty" << endl;
	}

	int getCount() {
		int count = 0;
		BiNode<T> *tmp = Head;
		while (tmp != NULL) {
			count++;
			tmp = tmp->Next;
		}
		return count;
	}

	void DeleteList() {
		BiNode<T> *tmp1 = Head;
		BiNode<T> *tmp2;

		while (tmp1 != NULL) {
			tmp2 = tmp1;
			tmp1 = tmp1->Next;
			delete tmp2;
		}

		Head = NULL;
	}

	void Print()
	{
		BiNode<T> *temp = Head;
		cout << "\nPrint list: " << endl;
		while (temp != NULL)
		{
			cout << temp->Key << " ";
			temp = temp->Next;
		}
		cout << endl << "" << endl;

	}

	void Swap(T _Key1, T _Key2)
	{
		BiNode<T> *node1 = Head;
		BiNode<T> *node2 = Head;

		while (node1->Key != _Key1) node1 = node1->Next;
		while (node2->Key != _Key2) node2 = node2->Next;

		T tmp = node1->Key;
		node1->Key = node2->Key;
		node2->Key = tmp;
	}

	

	void AddToEnd(T _Key) {
		BiNode<T> *tmp = Head;
		while (tmp->Next != NULL) tmp = tmp->Next;

		BiNode<T> *node = new BiNode<T>;
		node->Key = _Key;
		tmp->Next = node;
		node->Next = NULL;
		node->Prev = tmp;
	}

	void Sort() {

		BiNode<T> *node1;
		BiNode<T> *node2;

		for (int i = 0; i < getCount(); i++)
		{
			node1 = Head;
			node2 = node1->Next;
			while (node2 != NULL)
			{
				if (node1->Key > node2->Key)
				{
					Swap(node1->Key, node2->Key);
				}
				node1 = node1->Next;
				node2 = node2->Next;
			}
		}

	}

	void Merge(ListBi & B) {

		BiNode<T> *tmp = B.Head;

		while (tmp != NULL)
		{
			AddToEnd(tmp->Key);
			tmp = tmp->Next;
		}
	}
};



/*#pragma once
#include <iostream>
using namespace std;

template<typename T>
class Elem
{
	T Key; // ������
	Elem * Next, *Prev;
};
template<typename T>
class ListBi
{
	// ������, �����
	Elem <T> * Head, *End;
	// ���������� ���������
	int Count;

public:


	ListBi()
	{
		// ���������� ������ ����
		Head = End = NULL;
		Count = 0;
	}

	ListBi(const ListBi & L)
	{
		Head = End = NULL;
		Count = 0;

		// ������ ������, �� �������� ��������
		Elem * temp = L.Head;
		// ���� �� ����� ������
		while (temp != 0)
		{
			// ���������� ������
			AddEnd(temp->data);
			temp = temp->next;
		}
	}

	~ListBi()
	{
		// ������� ��� ��������
		DelAll();
	}

	void AddHead(int n)
	{
		// ����� �������
		Elem * temp = new Elem;

		// ����������� ���
		temp->prev = 0;
		// ��������� ������
		temp->data = n;
		// ��������� - ������ ������
		temp->next = Head;

		// ���� �������� ����?
		if (Head != 0)
			Head->Prev = temp;

		// ���� ������� ������, �� �� ������������ � ������ � �����
		if (Count == 0)
			Head = End = temp;
		else
			// ����� ����� ������� - ��������
			Head = temp;

		Count++;
	}

	void AddEnd(int n)
	{
		// ������� ����� �������
		Elem * temp = new Elem;
		// ���������� ���
		temp->next = 0;
		// ��������� ������
		temp->data = n;
		// ���������� - ������ �����
		temp->prev = End;

		// ���� �������� ����?
		if (End != 0)
			End->Next = temp;

		// ���� ������� ������, �� �� ������������ � ������ � �����
		if (Count == 0)
			Head = End = temp;
		else
			// ����� ����� ������� - ���������
			End = temp;

		Count++;
	}

	void Insert(int pos)
	{
		// ���� �������� ����������� ��� ����� 0, �� ����������� ���
		if (pos == 0)
		{
			cout << "Input position: ";
			cin >> pos;
		}

		// ������� �� 1 �� Count?
		if (pos < 1 || pos > Count + 1)
		{
			// �������� �������
			cout << "Incorrect position !!!\n";
			return;
		}

		// ���� ������� � ����� ������
		if (pos == Count + 1)
		{
			// ����������� ������
			int data;
			cout << "Input new number: ";
			cin >> data;
			// ���������� � ����� ������
			AddEnd(data);
			return;
		}
		else if (pos == 1)
		{
			// ����������� ������
			int data;
			cout << "Input new number: ";
			cin >> data;
			// ���������� � ������ ������
			AddHead(data);
			return;
		}

		// �������
		int i = 1;

		// ����������� �� ������ n - 1 ���������
		Elem * Ins = Head;

		while (i < pos)
		{
			// ������� �� ��������, 
			// ����� ������� �����������
			Ins = Ins->next;
			i++;
		}

		// ������� �� ��������, 
		// ������� ������������
		Elem * PrevIns = Ins->prev;

		// ������� ����� �������
		Elem * temp = new Elem;

		// ������ ������
		cout << "Input new number: ";
		cin >> temp->data;

		// ��������� ������
		if (PrevIns != 0 && Count != 1)
			PrevIns->next = temp;

		temp->next = Ins;
		temp->prev = PrevIns;
		Ins->prev = temp;

		Count++;
	}

	void Del(int pos)
	{
		// ���� �������� ����������� ��� ����� 0, �� ����������� ���
		if (pos == 0)
		{
			cout << "Input position: ";
			cin >> pos;
		}
		// ������� �� 1 �� Count?
		if (pos < 1 || pos > Count)
		{
			// �������� �������
			cout << "Incorrect position !!!\n";
			return;
		}

		// �������
		int i = 1;

		Elem * Del = Head;

		while (i < pos)
		{
			// ������� �� ��������, 
			// ������� ���������
			Del = Del->next;
			i++;
		}

		// ������� �� ��������, 
		// ������� ������������ ����������
		Elem * PrevDel = Del->prev;
		// ������� �� ��������, ������� ������� �� ���������
		Elem * AfterDel = Del->next;

		// ���� ������� �� ������
		if (PrevDel != 0 && Count != 1)
			PrevDel->next = AfterDel;
		// ���� ������� �� �����
		if (AfterDel != 0 && Count != 1)
			AfterDel->prev = PrevDel;

		// ��������� �������?
		if (pos == 1)
			Head = AfterDel;
		if (pos == Count)
			End = PrevDel;

		// �������� ��������
		delete Del;

		Count--;
	}

	void Print(int pos)
	{
		// ������� �� 1 �� Count?
		if (pos < 1 || pos > Count)
		{
			// �������� �������
			cout << "Incorrect position !!!\n";
			return;
		}

		Elem * temp;

		// ���������� � ����� ������� 
		// ������� ���������
		if (pos <= Count / 2)
		{
			// ������ � ������
			temp = Head;
			int i = 1;

			while (i < pos)
			{
				// ��������� �� ������� ��������
				temp = temp->next;
				i++;
			}
		}
		else
		{
			// ������ � ������
			temp = End;
			int i = 1;

			while (i <= Count - pos)
			{
				// ��������� �� ������� ��������
				temp = temp->prev;
				i++;
			}
		}
		// ����� ��������
		cout << pos << " element: ";
		cout << temp->data << endl;
	}

	void Print()
	{
		// ���� � ������ ������������ ��������, �� ��������� �� ����
		// � �������� ��������, ������� � ���������
		if (Count != 0)
		{
			Elem * temp = Head;
			cout << "( ";
			while (temp->next != 0)
			{
				cout << temp->data << ", ";
				temp = temp->next;
			}

			cout << temp->data << " )\n";
		}
	}

	void DelAll()
	{
		// ���� �������� ��������, ������� �� ������ � ������
		while (Count != 0)
			Del(1);
	}

	int GetCount()
	{
		return Count;
	}

	Elem * GetElem(int pos)
	{
		Elem *temp = Head;

		// ������� �� 1 �� Count?
		if (pos < 1 || pos > Count)
		{
			// �������� �������
			cout << "Incorrect position !!!\n";
			return 0;
		}

		int i = 1;
		// ���� ������ ��� �������
		while (i < pos && temp != 0)
		{
			temp = temp->next;
			i++;
		}

		if (temp == 0)
			return 0;
		else
			return temp;
	}


	// �������� ���� �������
	ListBi operator + (const ListBi& L)
	{
		// ������� �� ��������� ������ �������� ������� ������
		ListBi Result(*this);
		// List Result = *this;

		Elem * temp = L.Head;

		// ��������� �� ��������� ������ �������� ������� ������
		while (temp != 0)
		{
			Result.AddEnd(temp->data);
			temp = temp->next;
		}

		return Result;
	}
};*/