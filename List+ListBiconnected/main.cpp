#include <iostream>
#include "List.h"
#include "ListBiconnected.h"
using namespace std;

void ListTest() {
	List<int> A;
	List<int> B;
	cout << "Testing a list: " << endl;

	for (int i = 1; i < 11; i++) {
		A.Add(i);
	}
	for (int i = 11; i < 22; i++) {
		B.Add(i);
	}
	cout << "Create two lists" << endl;
	cout << "Put 10 elements in each list" << endl;
	cout << "Merged lists" << endl;
	A.Merge(B);
	A.Print();

	cout << "Sort the list and print" << endl;
	A.Sort();
	A.Print();

	cout << "Check for empty " << endl;
	A.checkEmpty();
	cout << "Delete list" << endl;
	A.DeleteList();
	cout << "Check for empty " << endl;
	A.checkEmpty();
}

void ListBiconnectedTest() {
	ListBi<int> A;
	ListBi<int> B;
	cout << "Testing a biconnected list: " << endl;

	for (int i = 1; i < 11; i++) {
		A.Add(i);
	}
	for (int i = 11; i < 22; i++) {
		B.Add(i);
	}
	cout << "Create two lists" << endl;
	cout << "Put 10 elements in each list" << endl;
	cout << "Merged lists" << endl;
	A.Merge(B);
	A.Print();

	cout << "Sort the list and print" << endl;
	A.Sort();
	A.Print();

	cout << "Check for empty " << endl;
	A.CheckEmpty();
	cout << "Delete list" << endl;
	A.DeleteList();
	cout << "Check for empty " << endl;
	A.CheckEmpty();
}

int main()
{
	


	int a;

	do {
		cout << "1 - Testing a list" << endl;
		cout << "2 - Testing a biconnected list" << endl;
		cout << "3 - Exit" << endl;

		cin >> a;
		if (a == 1) ListBiconnectedTest();
		else if (a == 2) ListTest();
		else a = 3;

	} while (a != 3);




}

/*
void DoubleLinkedListTest() {
	Double_Linked_List<int> A;
	Double_Linked_List<int> B;
	cout << "������������ ����������� ������: " << endl;

	for (int i = 1; i < 6; i++) {
		A.Add(i);
	}
	for (int i = 6; i < 11; i++) {
		B.Add(i);
	}
	cout << "������� ��� ������" << endl;
	cout << "�������� 5 ��������� � ������" << endl;
	cout << "��������� ������� �������" << endl;
	A.Merge(B);
	A.Show();

	cout << "��������� ������ � �������" << endl;
	A.Sort();
	A.Show();

	cout << "�������� �� ������� " << endl;
	A.CheckVoid();
	cout << "�������� ������" << endl;
	A.DeleteList();
	cout << "�������� �� ������� " << endl;
	A.CheckVoid();
}

int main()
{
	setlocale(LC_ALL, "Russian");


	int a;

	do {
		cout << "1 - ������������ ����������� ������" << endl;
		cout << "2 - ������������ ����������� ������" << endl;
		cout << "3 - �����" << endl;

		cin >> a;
		if (a == 1) DoubleLinkedListTest();
		else if (a == 2) ListTest();
		else a = 3;

	} while (a != 3);




}


/*#include "List.h"
#include "ListBiconnected.h"

int main() {


	List list;
	int newElem;
	cout << list.CheckEmpty() << endl;
	list.Add(13);
	list.Add(42);
	list.Add(2);
	list.Add(142);
	list.Add(18);
	list.Add(96);

	cout << "Number of elements in list: " << list.OutputNumber() << endl;
	cout << list.CheckEmpty() << endl;
	/*while (q != 0) {
	cout << "1. Input element." << endl;
	cout << "2. Show list. " << endl;
	cout << "3. Delete element. " << endl;
	cout << "4. Check. " << endl;
	cout << "0. Exit." << endl;
	cin >> q;
	switch (q) {
	case 0: q = 0;
	break;
	case 1: {cin >> newElem;
	list.Add(newElem);

	break; }
	case 2: list.Show(); break;

	case 3: list.Delete(); break;
	case 4: { list.Check(Node, Head);  break; }

	}
	}
	


ListBi L;

const int n = 10;
int a[n] = { 0,1,2,3,4,5,6,7,8,9 };

// ��������� ��������, ������� �� ������ ��������, � ������,
// �� �������� - � �����
for (int i = 0; i < n; i++)
	if (i % 2 == 0)
		L.AddHead(a[i]);
	else
		L.AddTail(a[i]);

// ���������� ������
cout << "List L:\n";
L.Print();

cout << endl;

// ������� �������� � ������
L.Insert();
// ���������� ������
cout << "List L:\n";
L.Print();

// ���������� 2-�� � 8-�� ��������� ������
L.Print(2);
L.Print(8);

ListBi T;

// �������� ������
T = L;
// ���������� �����
cout << "List T:\n";
T.Print();

// ���������� ��� ������ (������ � ������������ ���������)
cout << "List Sum:\n";
ListBi Sum = -L + T;
// ���������� ������
Sum.Print();






getchar();
return 0;
}*/