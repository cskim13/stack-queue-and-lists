#include <iostream>
#include <string>
#include "Stack.h"
#include "Queue.h"
using namespace std;

void StackTest() {

	Stack<int> MyStack(10);

	cout << "Testing the data structure of the stack:" << endl;
	cout << "Allocate memory for 10 elements" << endl;
	for (int i = 1; i < 11; i++) {
		MyStack.Push(i);
		cout << "Put value " << i << endl;
	}
	cout << "Overflow check:" << endl;
	cout << "Attempt to put value " << 11 << endl;
	MyStack.Push(11);

	for (int i = 1; i < 11; i++) {
		MyStack.Pop();
		cout << "Delete value " << i << endl;
	}
	cout << "Check for emptiness:" << endl;
	cout << "Attempt to delete value " << endl;
	MyStack.Pop();
}

void QueueTest() {

	Queue<int> MyQueue(10);

	cout << "Testing the queue data structure:" << endl;
	cout << "Allocate memory for 10 elements" << endl;
	for (int i = 1; i < 11; i++) {
		MyQueue.Push(i);
		cout << "Put value " << i << endl;
	}
	cout << "Overflow check:" << endl;
	cout << "Attempt to put value " << 11 << endl;
	MyQueue.Push(11);

	for (int i = 1; i < 11; i++) {
		MyQueue.Pop();
		cout << "Delete value " << i << endl;
	}
	cout << "Check for emptiness:" << endl;
	cout << "Attempt to delete value " << endl;
	MyQueue.Pop();
}

int main()
{
	int q;
	do {
		cout << "1 - Testing the data structure of the stack" << endl;
		cout << "2 - Testing the queue data structure" << endl;
		cout << "3 - Exit" << endl;
		cin >> q;
		if (q == 1) StackTest();
		else if (q == 2) QueueTest();
		else q = 3;
	}
	while (q != 3);
}





/*#pragma once
#include "Queue.h"
#include "Stack.h"
int main() {

	Stack <int> MyStack(10);
	MyStack.Push(3);
	MyStack.Push(8);
	MyStack.Push(15);
	MyStack.Push(44);
	MyStack.Print(MyStack);
	Queue <int> MyQueue(10);
	cout << "ret";
	MyQueue.Push(MyQueue, 3); 
	MyQueue.Push(MyQueue, 8);
	MyQueue.Push(MyQueue, 15);
	MyQueue.Push(MyQueue, 44);
	cout << "ret";
	MyQueue.Print(MyQueue);
	cout << "ret";
	
	
	getchar();
	return 0;
}*/