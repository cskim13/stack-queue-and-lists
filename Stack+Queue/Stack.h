#pragma once
template <class T>
class Stack {
private:
	T * Array;
	unsigned int Size;
	unsigned int Index;
public:

	Stack(int _size) : Size(_size), Index(0) {
		Array = new T[Size];
	}

	Stack(const Stack &myStack) : Size(2 * myStack.Size), Index(myStack.Index)	{
		Array = new T[Size];

		for (int i = 0; i < Index; i++) {
			Array[i] = myStack.Array[i];
		}
	}

	~Stack() {
		delete[] Array;
	}

	void Print() {
		cout << "----------------" << endl;
		for (int i = Index - 1; i >= 0; i--)
			cout << Array[i] << endl;
	}

	void Push(T Value) {
		if (Index < Size) Array[Index++] = Value;
		else cout << "Stack overflow" << endl;
	}
    
	T Pop() {
		if (Index > 0) {
			Index--;
			return Array[Index];
		}
		else cout << "Stack empty" << endl;
		return 0;
	}

	T Peek() {
		if (Index > 0) return Array[Index - 1];
		return 0;
	}

};

