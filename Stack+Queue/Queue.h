#pragma once
template <class T>
class Queue {
private:
	T * Array;
	unsigned int Size;
	unsigned int Start;
	unsigned int End;
	unsigned int Index;
public:
	Queue(int _size) : Size(_size), Index(0), End(0), Start(0) {
		Array = new T[Size];
	}

	Queue(const Queue &myQueue) : Start(0), End(myQueue.Size), Index(myQueue.Index), Size(2 * myQueue.Size) {
		Array = new T[Size];
		if (myQueue.End > myQueue.Start) {
			int q = 0;
			for (int i = myQueue.Start; i < myQueue.End; i++) {
				Array[q] = myQueue.Array[i];
				q++;
			}
			for (int i = myQueue.End; i < myQueue.Start; i++) {
				Array[q] = myQueue.Array[i];
				q++;
			}
		}
		else
		{
			int q = 0;
			for (int i = myQueue.Start; i < myQueue.Size; i++) {
				Array[q] = myQueue.Array[i];
				q++;
			}
			for (int i = 0; i < myQueue.End; i++) {
				Array[q] = myQueue.Array[i];
				q++;
			}
		}
		delete[] myQueue.Array;
	}

	void Print() {
		cout << "----------------" << endl;
		if (End > Start) {
			for (int i = Start; i < End; i++) {
				cout << Array[i] << endl;
			}
			for (int i = End; i < Start; i++) {
				cout << Array[i] << endl;
			}
		}
		else {
			for (int i = Start; i < Size; i++) {
				cout << Array[i] << endl;
			}
			for (int i = 0; i < End; i++) {
				cout << Array[i] << endl;
			}
		}

	}

	void Push(T Value) {
		if (Index < Size) {
			Array[End++] = Value;
			Index++;
			if (End >= Size) End = 0;
		}
		else cout << "Queue overflow" << endl;
	}

	T Pop() {
		if (Index > 0) {
			T returnValue = Array[Start++];
			Index--;
			if (Start >= Size) Start = 0;
			return returnValue;
		}
		else cout << "Queue empty" << endl;
		return 0;
	}

	T Peek() {
		if (Index > 0) return Array[Start];
		return 0;
	}



};

